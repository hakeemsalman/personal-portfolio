/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      // backgroundImage: {
      //   "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
      //   "gradient-conic":
      //     "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      // },
      colors: {
        primary: '#161A30', // Adjust the value to match your global variable
        secondary: '#2D9596',
        white: '#ECF4D6',
      },
      keyframes: {
        translation: {
          '0%': { transform: 'translateX(0px)' },
          '100%': { transform: 'translateX(calc( -100% + 100vw))' }
        }
      },
      animation: {
        'translate-text': 'translation 4s linear infinite alternate',
      },
    },
  },
  plugins: [],
};
