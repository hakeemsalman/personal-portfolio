"use client"
import { Box, Button, Card, CardActions, CardContent, CardMedia, Chip, Link, Typography } from '@mui/material';
import Image from 'next/image';
import { useEffect, useState } from 'react';

function Projects () {
  const [loading, setLoading] = useState(false);
  const [datam, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        let res;
        await fetch('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/project.json').then(response => response.json()).then(data => res = data);
        setData(res);

        // const response = await axios.get('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/project.json');
        // setData(response.data);

        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();

  }, []);



  return (
    <div className='w-full hero-bg flex justify-center'>

    <div className="wrapper ">
      <div className='projects min-h-svh mx-4 py-5 lg:mx-24 lg:py-20'>
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8 p-4">
  {loading ? (
    <h2 className=" animate-pulse text-center">Loading...</h2>
  ) : (
    datam.map((item) => (
      <div className="max-w-md mx-auto bg-white rounded-xl overflow-hidden shadow-lg" key={item.id}>
        <Image className="h-48 w-full object-cover"  src={item['cover-photo-url']} alt="Cover" width={100} height={100}/>
        <div className="p-6">
          <h3 className="text-xl font-medium text-gray-900">{item.title}</h3>
          <p className="text-gray-600 mt-2">{item.description}</p>
          <div className="mt-4 flex flex-wrap gap-2">
            {item.tech.map((techItem) => (
              <span key={techItem} className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700">{techItem}</span>
            ))}
          </div>
          <div className="mt-4">
            <a href={item['link-url']} target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">View</a>
            <a href={item['github-url']} target="_blank" rel="noopener noreferrer" className="ml-4 text-blue-500 hover:underline">Code</a>
          </div>
        </div>
      </div>
    ))
  )}
</div>

      </div>
    </div>
  </div>
  )
}

export default Projects;