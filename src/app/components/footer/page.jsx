"use client"
import { Tooltip } from '@mui/material';
import { socialLinks } from '../../data';
import Image from 'next/image';

function Footer(){

  return (
    <div className=' text-primary bg-white p-7 text-xl'>
      <div className="wrapper m-auto">
        <div className="flex items-center justify-between flex-col  md:flex-row gap-5">
          <div className='text-base sm:text-lg md:text-2xl' >Made with <span >❤️</span> by Hakeem Salman</div>
          <div className="flex items-center justify-between gap-5 flex-row flex-nowrap">
            {
              socialLinks.map(item => (
                <Tooltip title={item.name} placement="top" key={item.id}>
                  <a href={item.url} target='_blank'>

                  <Image 
                    src={item.path}
                    alt={item.name}
                    width={30}
                    height={30}/>
                    </a>
                </Tooltip>
              ))
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer;
