import { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { menu } from '@/app/data'
import { inter } from '@/app/fonts';
import clsx from 'clsx';
import { usePathname } from 'next/navigation';


const MobileNavbar = () => {
  const [isActive, setIsActive] = useState(false);
  const pathname = usePathname();

  const toggleNavbar = () => {
    setIsActive(!isActive);
  };

  return (
    <div className='md:hidden'>
      <div className="flex items-center">
      <button onClick={toggleNavbar} 
  className="flex flex-col justify-center items-center relative z-50">
    <span className={`bg-black block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm ${isActive ? 'rotate-45 translate-y-1' : '-translate-y-0.5'
                    }`} >
    </span>
    <span className={`bg-black block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm my-0.5 ${isActive ? 
                    'opacity-0' : 'opacity-100'
                    }`}>
    </span>
    <span className={`bg-black block transition-all duration-300 ease-out 
                    h-0.5 w-6 rounded-sm ${isActive ? 
                    '-rotate-45 -translate-y-1' : 'translate-y-0.5'
                    }`} >
    </span>    

  </button>
        {isActive && (
          <div className="absolute left-0 transition-all duration-300 ease-out top-0 h-full w-1/2 bg-white z-20">
            <div className='flex flex-col py-10'>

            {menu.map((item) => (
              <Link key={item.id} href={item.url} className={clsx(
                `${inter.className} text-primary text-lg py-2 px-2`,
                {
                  'font-bold' : pathname === item.url,
                }
              )}>
                {item.name}
              </Link>
            ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default MobileNavbar;

