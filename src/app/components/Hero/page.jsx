'use client'
import { Tooltip } from '@mui/material'
import { cloudTechStackList, webTtechStackList } from '@/app/data';
import Image from 'next/image';
import { inter } from '@/app/fonts';
import { mulish } from '@/app/fonts';

function Hero() {
  return (
    <>
      <header className='hero-bg relative'>
        <div className='flex wrapper m-auto justify-between items-center w-full py-8 px-5 md:px-36 md:py-28 lg:flex-row flex-col gap-20 lg:gap-0 relative z-1'>
          <div>
            <p className={`${inter.className} text-white md:text-6xl text-3xl font-bold`}><span className='duration-300 ease-in-out hover:text-orange-400'>Application&nbsp;</span><span className='and duration-300 ease-in-out font-thin hover:text-pink-500'>and&nbsp;</span></p>
            <p className={`${inter.className} text-white md:text-6xl text-3xl font-bold`}><span className='duration-300 ease-in-out hover:text-emerald-500'>Web</span> <span className='dev duration-300 ease-in-out hover:text-red-500'>developer</span></p>
          </div>
          <Image
            className=' hover:scale-125 relative z-10 md:w-96 md:h-96'
            src="/profile.svg"
            width={250}
            height={250}
            alt="Profile photo"
          />
        </div>
        <div className={`${mulish.className} absolute top-96 text-lime-200 -z-0 text-opacity-20 text-center animate-translate-text text-5xl md:text-9xl scale-125`}>
          <p className=''>Developer</p>
        </div>
      </header>
      <section className='bg-white'>
        <div className="wrapper m-auto">
          <div className="flex items-center justify-around flex-col flex-nowrap gap-5">
            <div className='flex items-center justify-center gap-10 flex-col flex-nowrap px-12 py-40 w-full'>
              <div className=' flex items-center justify-between flex-col lg:flex-row gap-10 flex-nowrap w-full'>
                <div className='flex flex-wrap items-start justify-center gap-5 flex-col '>
                  <h1 className={`${inter.className} font-bold text-2xl md:text-4xl lg:text-4xl xl:text-6xl  text-primary `}><span className='hover:text-red-500'>Web</span><span className='font-thin hover:font-normal'>&nbsp;Development</span></h1>
                  <div className="flex items-start justify-center gap-3 flex-col flex-wrap">
                    <p className='flex items-center justify-center gap-3'>
                      <Image
                        className='hover:scale-125'
                        src="/symbol.svg"
                        width={20}
                        height={20}
                        alt="symbol" />
                      <span className='subtitle'>Building responsive website front end using NextJS & Tailwind</span>
                    </p>
                    <p className='flex items-center justify-center gap-3'>
                      <Image
                        className='hover:scale-125'
                        src="/symbol.svg"
                        width={20}
                        height={20}
                        alt="symbol" />
                      <span className='subtitle'>Creating application backend in Node & Express</span>
                    </p>
                  </div>
                </div>
                <Image
                  className='hover:scale-125'
                  src="/thumbnail1.svg"
                  width={600}
                  height={600}
                  alt="man showing pic" />
              </div>
              <div className='flex items-center justify-center gap-5 flex-row flex-wrap'>
                {
                  webTtechStackList.map(item => (
                    <Tooltip title={item.name} key={item.id} placement="top">
                      <Image
                        src={item.url}
                        alt={item.name}
                        width={100}
                        height={100} />
                    </Tooltip>
                  ))
                }
              </div>
            </div>
            <div className='flex items-center justify-center gap-10 flex-col flex-nowrap px-12 py-40 w-full' >
              <div className='flex items-center justify-between flex-col lg:flex-row gap-10 flex-nowrap w-full'>
                <div className='flex items-start justify-center gap-5 flex-col flex-nowrap'>
                  <h1 className={`${inter.className} font-bold text-2xl md:text-4xl lg:text-4xl xl:text-6xl text-primary`} ><span className='hover:text-red-500'>Cloud Infra</span> <span className='font-thin hover:font-normal'>-Arch</span></h1>
                  <div className="flex items-start justify-center gap-3 flex-col flex-nowrap">
                    <p className='flex items-center justify-center gap-3'>
                        <Image
                          className='hover:scale-125'
                          src="/symbol.svg"
                          width={20}
                          height={20}
                          alt="symbol" />
                      <span className='subtitle'>Experience building enterprise applications on Salesforce Platform</span>
                    </p>
                    <p className='flex items-center justify-center gap-3'>
                        <Image
                          className='hover:scale-125'
                          src="/symbol.svg"
                          width={20}
                          height={20}
                          alt="symbol" />
                      <span className='subtitle'>Worked on various Database & Cloud Platform</span>
                    </p>
                  </div>
                </div>
                <div >
                  <Image
                  className='hover:scale-125'
                    src="/thumbnail2.svg"
                    alt="man showing pic"
                    width={500}
                    height={500}
                  />
                </div>
              </div>
              <div className='flex items-center justify-center gap-5 flex-row flex-wrap'>
                {
                  cloudTechStackList.map(item => (
                    <Tooltip title={item.name} key={item.id} placement="top">
                      <Image
                        src={item.url}
                        width={100}
                        height={100}
                        alt={item.name}
                      />
                    </Tooltip>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Hero;

