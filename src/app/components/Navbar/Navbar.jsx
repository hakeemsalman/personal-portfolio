'use client'
import React,{useState}from 'react'
import { menu } from '@/app/data'
import Link from 'next/link';
import { montserrat } from '@/app/fonts';
import { inter } from '@/app/fonts';
import { usePathname } from 'next/navigation';
import clsx from 'clsx';
import MobileNavbar from '../MobileNavbar/MobileNavbar';

// import './navbar.scss';

// import {MobileNavbar } from '../MobileNavbar/MobileNavbar'

export const Navbar = () => {
  const pathname = usePathname();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <nav className='w-full flex justify-between items-center flex-row flex-nowrap px-5 py-3 md:px-16 md:py-3 '>
        <Link href="/" className={`${montserrat.className} text-primary text-3xl`}>Salman<span className={`${inter.className} text-secondary text-4xl` }>.</span><span className={`${inter.className} text-white text-4xl`}>_</span></Link>
        <div className='flex justify-between items-center'>
          <ul className='md:flex md:flex-row md:justify-between md:items-center md:gap-8 md:flex-nowrap hidden'>
            {menu.map((item) => (
              <Link key={item.id} href={item.url} className={clsx(
                `${inter.className} text-primary text-lg`,
                {
                  'font-bold' : pathname === item.url,
                }
                )}>
                <li>{item.name}</li>
              </Link>
            ))}
          </ul>
            <MobileNavbar/>
        </div>
      </nav>
      {/* <div id="navbar-default hide">
        <ul className={isOpen ? 'mobile-nav-links open' : 'mobile-nav-links'}>
          {menu.map((item) => (
            <NavLink key={item.id} to={item.url} className="menu-item">
              <li>{item.name}</li>
            </NavLink>
          ))}
        </ul>
      </div> */}

    </>
  )
}
