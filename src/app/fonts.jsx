import { Inter } from 'next/font/google';
import { Montserrat  } from 'next/font/google';
import { Mulish  } from 'next/font/google';
 
export const inter = Inter({ subsets: ['latin'] });
export const montserrat = Montserrat({subsets: ['latin'], weight:'400'});
export const mulish = Mulish({subsets: ['latin'], weight:'700'});