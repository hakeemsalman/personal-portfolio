import { Inter } from "next/font/google";
import "./globals.css";
import { Navbar } from "@/app/components/Navbar/Navbar";
import Footer from "@/app/components/footer/page";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Salman Portfolio",
  description: "This is a Portfolio, built with Love",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Navbar/>
        {children}
        <Footer/>
        </body>
    </html>
  );
}
