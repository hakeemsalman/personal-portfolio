"use client"
import React from 'react';
import {  Tooltip } from '@mui/material';
import Image from 'next/image';

const Contact = () => {

  return (
    <div className="hero-bg p-10 flex justify-center items-center flex-col min-h-svh">
      <div className="contact-section wrapper">
        <h1 className='text-white text-lg text-center md:text-4xl'>Feel free to reach out to me on social media! <br/>📱 <br />Let&apos;s connect!</h1>
      </div>
    </div>
  );
};

export default Contact;

