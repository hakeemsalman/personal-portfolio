"use client"
export const menu = [
  {
    id: 1,
    url: "/",
    name: "Home",
  },
  {
    id: 2,
    url: "/projects",
    name: "Projects",
  },
  {
    id: 3,
    url: "/certification",
    name: "Certification",
  },
  {
    id: 4,
    url: "/contact",
    name: "Contact",
  },
];

export const cloudTechStackList = [
  {
    id: 1,
    name: 'salesforce',
    url: '/tech/salesforce.svg'
  },
  {
    id: 2,
    name: 'aws',
    url: '/tech/aws.svg'
  },
  {
    id: 3,
    name: 'firebase',
    url: '/tech/firebase.svg'
  },
  {
    id: 4,
    name: 'mysql',
    url: '/tech/mysql.svg'
  },

];

export const socialLinks = [
  {
    id: 1,
    name: 'github',
    path: '/tech/github.svg',
    url: 'https://github.com/hakeemsalman',
  },
  {
    id: 2,
    name: 'linkedin',
    path: '/tech/linkedin.svg',
    url: 'https://linkedin.com/in/hakeem-salman'
  },
  {
    id: 3,
    name: 'instagram',
    path: '/tech/instagram.svg',
    url: 'https://instagram.com/',
  },
]


export const webTtechStackList = [
  {
    id: 1,
    name: 'HTML',
    url: '/tech/html.svg'
  },
  {
    id: 2,
    name: 'CSS',
    url: '/tech/css.svg'
  },
  {
    id: 3,
    name: 'Bootstrap',
    url: '/tech/boostrap.svg'
  },
  {
    id: 4,
    name: 'Javascript',
    url: '/tech/javascript.svg'
  },
  {
    id: 5,
    name: 'nodejs',
    url: '/tech/node.svg'
  },
  {
    id: 6,
    name: 'React',
    url: '/tech/react.svg'
  },
  {
    id: 7,
    name: 'npm',
    url: '/tech/npm.svg'
  },
  {
    id: 8,
    name: 'Tailwind CSS',
    url: '/tech/tailwind.svg'
  },
];

