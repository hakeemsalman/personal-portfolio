"use client"
import { Box, IconButton, ImageList, ImageListItem, ImageListItemBar, Link, Tooltip, Typography } from '@mui/material';
// import './certification.scss';
import { certData, badgeData } from '../data';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Image from 'next/image';


function Certificate()  {

  const [loading, setLoading] = useState(false);
  const [centificationData, setData] = useState([]);
  const [badgeDate, setBadgeData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await axios.get('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/certification.json');
        const badge = await axios.get('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/badge.json');

        setBadgeData(badge.data);
        setData(response.data);
        // let res;
        // const response = await fetch('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/certification.json');
        // response.then(res => res.json()).then(data => res = data );
        
        // let badgeRes;
        // const badge = await fetch('https://raw.githubusercontent.com/hakeemsalman/hakeemsalman/main/data/badge.json');
        // badge.then(res => res.json()).then(data => badgeRes = data);
        
        // setBadgeData(badgeRes);
        // setData(res);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();

  }, []);


  return (
    <div className='w-full hero-bg flex justify-center'>

    <div className="wrapper">
      <div className=' min-h-svh my-20'>
        <div className="p-3">
          <Box sx={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr', padding: '10px', justifyItems: 'center', gridGap: '20px', flexWrap: 'wrap', padding: '10px', alignItems: 'center', }}>
            {loading ? <h2 className='text-white animate-pulse text-6xl'>Loading...</h2> : badgeDate.map(item => (
              <Tooltip title={item.title} key={item.id} placement="top">
                <Link href={`#${item.id}`} >
                  <div >
                    <Box
                      key={item.id}
                      component="img"
                      sx={{
                        width: 150,
                      }}
                      alt="The house from the offer."
                      src={item['cover-photo-url']}
                    />
                  </div>
                </Link>
              </Tooltip>
            ))}
          </Box>
        </div>
        <div className="flex flex-col items-center px-8 py-3 gap-[100px] flex-[1_0_0%] self-stretch">
          {loading ? <h2 className='text-white text-6xl'>Loading...</h2> : centificationData.map(item => (
            <div className="w-4/5" key={item.id}>
              <Image id={item.id} src={item['photo-url']} width={1000} height={600} alt="" />
            </div>
          ))}
        </div>
      </div>
    </div >
          </div>
  )
}

export default Certificate;
